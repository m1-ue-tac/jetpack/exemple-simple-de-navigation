package com.journaldev.androidjetpacknavigation;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

public class SecondFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.navigation_second_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d("JCT", "Paramètre reçu par le fragment 1 : " + SecondFragmentArgs.fromBundle(getArguments()).getDeF1AF2());

        Button button = view.findViewById(R.id.button_frag2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SecondFragmentDirections.ActionSecondFragmentToFirstFragment retour = SecondFragmentDirections.actionSecondFragmentToFirstFragment();
                retour.setDeF2AF1(true);
                Navigation.findNavController(view).navigate(retour);

                // code juste pour exemple d'actions possibles
//                final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);
//
//                navController.navigateUp();
//
//                navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
//                    @Override
//                    public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
//                        Log.d("JCT", destination.getLabel() + " ");
//                    }
//                });
            }
        });
    }

}
